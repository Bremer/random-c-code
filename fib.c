#include <stdio.h>
#include <stdlib.h>

int fib(int n);

int main(int argc, char *argv[])
{
    int i = 0;
    int number;

    if (argc < 2)
    {
        printf("Usage: %s <number>\n", argv[0]);
        return 1;
    }
    else if (argv[1][0] - '0' == 0)
    {
        printf("0 is not a number in the fibonacci sequence\n");
        return 1;
    }
    else if (!atoi(argv[1]))
    {
        printf("%s is not an integer\n", argv[1]);
        return 1;
    }

    number = atoi(argv[1]);

    for(; fib(i) <= number; i++)
    {
        if(fib(i) == number)
        {
            printf("True, %d is a number in the fibonacci sequence\n", number);
            return 0;
        }
    }

    printf("False, %d is not a number in the fibonacci sequence\n", number);

    return 0;
}

int fib(int n)
{
    if (n <= 1)
        return n;
    return fib(n - 1) + fib(n -2);
}
