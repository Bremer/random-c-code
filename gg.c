#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct node {
    int score;
    char name[20];
    struct node *next;
};

void print_list(struct node *head);
void insert(int score, char *name, struct node **head);
void swap(struct node *a, struct node *b);
void bubble_sort(struct node *start);
void free_allocated_memory(struct node *head);
int player_guess_number();

int main()
{
    struct node *head = NULL;
    char yn;
    char name[20];
    int again = 0;
    int guess_number, game, tries, play_again;

    do
    {
        play_again = 1;
        game = 1;
        tries = 0;

        srand(time(NULL));
        guess_number = rand() % 101;

        printf("Welcome to the guessing game. Input a number between 0-100.\n");

        do
        {
            game = player_guess_number(guess_number);
            tries++;
        } while (game);

        printf("Whats your name?\n");
        scanf("%s", name);
        printf("Done, %s completed it in %d tries.\n", name, tries);

        insert(tries, name, &head);

        while ((getchar()) != '\n');

        while (play_again)
        {
            printf("Play again? y/n\n");
            scanf("%c", &yn);

            while ((getchar()) != '\n');

            if (yn == 'y')
            {
                again = 1;
                play_again = 0;
            }
            else if (yn == 'n')
            {
                again = 0;
                play_again = 0;
            }
        };

    } while (again);

    bubble_sort(head);
    print_list(head);

    free_allocated_memory(head);



    return 0;
}

int player_guess_number(int guess_number)
{
    int player_guess, status;

    printf("Guess a number:\n");

    status = scanf("%d", &player_guess);

    while (status !=1)
    {

        while ((getchar()) != '\n');
        printf("Not an integer, type a number between 0-100\n");

        return 1;
    }

    if (player_guess == guess_number)
    {
        printf("Correct!\n");
        return 0;
    }
    else if (player_guess > 100)
    {
        printf("To high, guess a number between 0-100\n");
        return 1;
    }
    else if (player_guess < 0)
    {
        printf("To low, guess a number between 0-100\n");
        return 1;
    }
    else if (player_guess > guess_number)
    {
        printf("Wrong! To high.\n");
        return 1;
    }
      else if (player_guess < guess_number)
    {
        printf("Wrong! To low.\n");
        return 1;
    }

    return 0;
}

void free_allocated_memory(struct node *ptr)
{
    while(ptr != NULL)
    {
        struct node *free_ptr = ptr;
        ptr = ptr->next;
        free(free_ptr);
    }
}

void print_list(struct node *ptr)
{
    printf("====================\n");
    printf("**** HIGH SCORE ****\n");
    printf("====================\n");
    while (ptr != NULL)
    {
        printf("%d \t %s\n", ptr->score, ptr->name);
        ptr = ptr->next;
    }
    printf("====================\n");
}

void insert(int score, char *name, struct node **head)
{
    struct node *link = calloc(1, sizeof(struct node));
    link->score = score;
    strncpy(link->name, name, strlen(name));
    link->next = *head;
    *head = link;
}

void bubble_sort(struct node *start)
{
    int swapped;
    struct node *ptr1 = NULL;
    struct node *lptr = NULL;

    if (start == NULL)
    {
        return;
    }
    do
    {
        swapped = 0;
        ptr1 = start;

        while(ptr1->next != lptr)
        {
            if (ptr1->score > ptr1->next->score)
            {
                swap(ptr1, ptr1->next);
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    }
    while(swapped);
}

void swap(struct node *a, struct node *b)
{
    char name[20];
    int temp_score = a->score;

    a->score = b->score;
    b->score = temp_score;

    memset(name, 0 , 20);

    strncpy(name, a->name, sizeof(a->name));
    memset(a->name, 0, 20);
    strncpy(a->name, b->name, sizeof(b->name));
    memset(b->name, 0, 20);
    strncpy(b->name, name, sizeof(name));
}
