#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
    int score;
    char name[20];
    struct node *next;
};

void print_list(struct node *head);
void insert(int score, char *name, struct node **head);
void swap(struct node *a, struct node *b);
void bubble_sort(struct node *start);
void free_allocated_memory(struct node *head);

int main()
{

    struct node *head = NULL;

    insert(10, "josefin", &head);
    insert(20, "dennis", &head);
    insert(30, "olof", &head);
    insert(1, "rickard", &head);
    insert(40, "anna", &head);
    insert(56, "arne", &head);

    bubble_sort(head);
    print_list(head);

    free_allocated_memory(head);

    return 0;
}

void free_allocated_memory(struct node *ptr)
{
    while(ptr != NULL)
    {
        struct node *free_ptr = ptr;
        ptr = ptr->next;
        free(free_ptr);
    }
}

void print_list(struct node *ptr)
{
    printf("====================\n");
    printf("**** HIGH SCORE ****\n");
    printf("====================\n");
    while (ptr != NULL)
    {
        printf("%d \t %s\n", ptr->score, ptr->name);
        ptr = ptr->next;
    }
    printf("====================\n");
}

void insert(int score, char *name, struct node **head)
{
    struct node *link = calloc(1, sizeof(struct node));
    link->score = score;
    strncpy(link->name, name, strlen(name));
    link->next = *head;
    *head = link;
}

void bubble_sort(struct node *start)
{
    int swapped;
    struct node *ptr1 = NULL;
    struct node *lptr = NULL;

    if (start == NULL)
    {
        return;
    }
    do
    {
        swapped = 0;
        ptr1 = start;

        while(ptr1->next != lptr)
        {
            if (ptr1->score > ptr1->next->score)
            {
                swap(ptr1, ptr1->next);
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    }
    while(swapped);
}

void swap(struct node *a, struct node *b)
{
    char name[20];
    int temp_score = a->score;

    a->score = b->score;
    b->score = temp_score;

    memset(name, 0 , 20);

    strncpy(name, a->name, sizeof(a->name));
    memset(a->name, 0, 20);
    strncpy(a->name, b->name, sizeof(b->name));
    memset(b->name, 0, 20);
    strncpy(b->name, name, sizeof(name));
}
